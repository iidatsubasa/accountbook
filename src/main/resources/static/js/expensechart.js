$(function() {
	var category = document.getElementsByClassName("expense_category");
	var category_list = [];
	var categories = Array.prototype.slice.call(category);
	for(var i = 0; i < categories.length; i++){
		category_list.push(category[i].innerText);
	}

	var amount = document.getElementsByClassName("expense_amount");
	var amount_list = [];
	var amounts = Array.prototype.slice.call(amount);
	for(var i = 0; i < amounts.length; i++){
		amount_list.push(amount[i].innerText);
	}

	let container = $(".expense-chart")
	let ctx = $("#expensePieChart")
	ctx.attr("width", 300);
	var myChart = new Chart(ctx, {
	    type: 'pie',
	    data: {
	        labels: category_list,
	        datasets: [{
	            label: '# of Votes',
	            data: amount_list,
	            borderWidth: 1
	        }]
	    },
	    options: {
	    	responsive: true,
	    	maintainAspectRatio: false,
	    	title: {
	    		display: true,
	    		fontSize: 20,
	    		text: '支出'
	    	},
	        layout: { //レイアウト
	            padding: { //余白設定
	                left: 0,
	                right: 0,
	                top: 0,
	                bottom: 0
	            }
	        },
	        plugins: {
	            colorschemes: {
	                scheme: 'tableau.Classic20'
	            }
	        },
	    }
	});
});
