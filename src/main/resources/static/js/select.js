/**
 *
 */
	$(function() {
		var flag = document.getElementById("selflag")
		if(flag.value == 0){
			$('[name=majorCategoryId]').prop("selectedIndex", 0);
			$('[name=subCategoryId]').prop("selectedIndex", 0);
		}

		var $children = $('.children');
		var original = $children.html();

		$('.parent').change(function() {

			var val1 = $(this).val();
			var stringCountPar = val1.length;

			$children.html(original).find('option').each(function() {
				var val2 = $(this).val();
				var stringCountChi = val2.length;
				var leftval = val2.slice(0, stringCountPar);

				if ((stringCountPar == 1 && stringCountChi == 3) || (stringCountPar == 2 && stringCountChi == 4)){
				    if (val1 != leftval) {
	     				 $(this).not(':first-child').remove();
	  				}
	  			} else {
	  				 $(this).not(':first-child').remove();
	  			}
			});
		});
	});

	// 親セレクトボックス選択後、子セレクタボックス選択可能
	$(function() {
		var $childIncome = $('#child-income');

		$('#parent-income').change(function() {
			if($(this).val() == "") {
				$childIncome.attr('disabled', 'disabled');
			} else {
				$childIncome.removeAttr('disabled');
			}
		});
	});

	$(function() {
		var $childExpense = $('#sel-ex-sub');

		$('#sel-ex-major').change(function() {
			if($(this).val() == "") {
				$childExpense.attr('disabled', 'disabled');
			} else {
				$childExpense.removeAttr('disabled');
			}
		});
	});