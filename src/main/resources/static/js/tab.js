$(function(){
	let balance_flag = document.getElementById("balanceflag")
	if(balance_flag.value == 0){
		$('.income-tab, .income-form').addClass('active');
		$('.expense-tab, .expense-form').removeClass('active');
	}
});



$('.tab_box .tab_btn').click(function() {
	var index = $('.tab_box .tab_btn').index(this);
	$('.tab_box .tab_btn, .tab_box .add_form').removeClass('active');
	$(this).addClass('active');
	$('.tab_box .add_form').eq(index).addClass('active');
});