/**
 *
 */

	$(function() {
		var $childSort = $('#sortChild');

		$('#sortParent').change(function() {
			if($(this).val() == "") {
				$childSort.attr('disabled', 'disabled');
			} else {
				$childSort.removeAttr('disabled');
			}
		});
	});

	$(function(){
		$('.money').each(function(index) {
		    let txt = $(this).text();
		    let num = Number(txt).toLocaleString();
		    $(this).text(
		        txt.replace(txt, num)
		    );
		});
	});

	$(function(){
		$("#balance-table").DataTable({
			language: {
	            url: "http://cdn.datatables.net/plug-ins/9dcbecd42ad/i18n/Japanese.json",
	        },
	        // 件数切替機能 無効
	        lengthChange: false,
	        // 検索機能 無効
	        searching: false,
	        // ソート機能 無効
	        ordering: false,
	        // 情報表示 無効
	        info: true,
	        // ページング機能 無効
	        paging: true,
	        order: [],
	        stateSave: true
		});
	});