package com.example.accountbook.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.example.accountbook.entity.UserSignupForm;

@Repository
public interface SignupUserRepository extends JpaRepository<UserSignupForm, Integer> {
	UserSignupForm findByAccount(String account);
}
