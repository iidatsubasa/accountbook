package com.example.accountbook.repository;

import java.util.Date;
import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import com.example.accountbook.entity.ExtendedBalance;

@Repository
public interface ExtendedBalanceRepository extends JpaRepository<ExtendedBalance, Integer> {

	@Query(value = "SELECT balances.id, balances.user_id,balances.amount, major_categories.major_name, categories.sub_name, balances.flag, balances.accrual_date, balances.memo "
			+ "FROM balances INNER JOIN categories  "
			+ "ON balances.sub_category_id = categories.sub_category_id  "
			+ "INNER JOIN major_categories "
			+ "ON balances.major_category_id = major_categories.major_category_id "
			+ "WHERE balances.user_id = ?1 AND balances.accrual_date "
			+ "BETWEEN ?2 AND ?3 "
			+ "ORDER BY balances.accrual_date DESC", nativeQuery = true)
	List<ExtendedBalance> findAll(int userId, Date since, Date until);

	@Query(value = "SELECT balances.id, balances.user_id,balances.amount, major_categories.major_name, categories.sub_name, balances.flag, balances.accrual_date, balances.memo "
			+ "FROM balances INNER JOIN categories  "
			+ "ON balances.sub_category_id = categories.sub_category_id  "
			+ "INNER JOIN major_categories "
			+ "ON balances.major_category_id = major_categories.major_category_id "
			+ "WHERE balances.user_id = ?1 AND balances.sub_category_id = ?2 AND balances.accrual_date "
			+ "BETWEEN ?3 AND ?4 "
			+ "ORDER BY balances.accrual_date DESC", nativeQuery = true)
	List<ExtendedBalance> findAllWhereSubCategory(int uesrId, Integer subCategoryId, Date since, Date until);

	@Query(value = "SELECT balances.id, balances.user_id,balances.amount, major_categories.major_name, categories.sub_name, balances.flag, balances.accrual_date, balances.memo "
			+ "FROM balances INNER JOIN categories  "
			+ "ON balances.sub_category_id = categories.sub_category_id  "
			+ "INNER JOIN major_categories "
			+ "ON balances.major_category_id = major_categories.major_category_id "
			+ "WHERE balances.user_id = ?1 AND balances.major_category_id = ?2 AND balances.accrual_date "
			+ "BETWEEN ?3 AND ?4 "
			+ "ORDER BY balances.accrual_date DESC", nativeQuery = true)
	List<ExtendedBalance> findAllWhereMajorCategory(int uesrId, Integer MajorCategoryId, Date since, Date until);

}
