package com.example.accountbook.repository;

import java.util.Date;
import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import com.example.accountbook.entity.Balance;

@Repository
public interface BalanceRepository extends JpaRepository<Balance, Integer>{

	@Query(value = "SELECT balances.major_category_id AS majorCategoryId, major_categories.major_name as majorCategoryName, SUM(balances.amount) as amount "
			+ "FROM balances INNER JOIN major_categories  "
			+ "ON balances.major_category_id = major_categories.major_category_id  "
			+ "WHERE balances.user_id = ?1 AND balances.flag = ?2 AND balances.accrual_date "
			+ "BETWEEN ?3 AND ?4 "
			+ "GROUP BY balances.major_category_id, major_categories.major_name "
			+ "ORDER BY amount DESC;", nativeQuery = true)
	List<BalanceInterface> findByUserIdGroupByCategoryIdEx(int userId, int flag, Date since, Date until);

	@Query(value = "SELECT balances.sub_category_id AS majorCategoryId, categories.sub_name as majorCategoryName, SUM(balances.amount) as amount "
			+ "FROM balances INNER JOIN categories  "
			+ "ON balances.sub_category_id = categories.sub_category_id  "
			+ "WHERE balances.user_id = ?1 AND balances.flag = ?2 AND "
			+ "(balances.accrual_date BETWEEN ?3 AND ?4 ) "
			+ "GROUP BY balances.sub_category_id, categories.sub_name "
			+ "ORDER BY amount DESC;", nativeQuery = true)
	List<BalanceInterface> findByUserIdGroupByCategoryIdIn(int userId, int flag, Date since, Date until);

	public interface BalanceInterface {
	    Integer getMajorCategoryId();
	    String getMajorCategoryName();
	    Integer getAmount();
	}

	@Query(value = "SELECT SUM(amount) as amount "
			+ "FROM balances WHERE user_id = ?1 AND flag = ?2 "
			+ "AND accrual_date BETWEEN ?3 AND ?4 ;", nativeQuery = true)
	Integer findAmount(int userId, int flag, Date since, Date until);
}

