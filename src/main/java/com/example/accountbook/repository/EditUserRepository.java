package com.example.accountbook.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.example.accountbook.entity.UserEditForm;

@Repository
public interface EditUserRepository extends JpaRepository<UserEditForm, Integer> {
	UserEditForm findByAccount(String account);
}
