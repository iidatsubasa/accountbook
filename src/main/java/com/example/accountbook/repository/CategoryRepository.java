package com.example.accountbook.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import com.example.accountbook.entity.Category;

@Repository
public interface CategoryRepository extends JpaRepository<Category, Integer>{

	@Query(value = "SELECT major_category_id FROM categories", nativeQuery = true)
	List<Integer> findByMajorCategoryId();

	@Query(value = "SELECT major_name FROM categories", nativeQuery = true)
	List<String> findByMajorName();

	@Query(value = "SELECT sub_category_id FROM categories", nativeQuery = true)
	List<Integer> findBySubCategoryId();

	@Query(value = "SELECT sub_name FROM categories", nativeQuery = true)
	List<String> findBySubName();


}
