package com.example.accountbook.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.example.accountbook.entity.LoginUser;

@Repository
public interface LoginUserRepository extends JpaRepository<LoginUser, Integer> {

	LoginUser findByUserName(String userName);

}
