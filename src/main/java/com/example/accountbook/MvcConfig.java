package com.example.accountbook;

import org.springframework.context.annotation.Configuration;
import org.springframework.web.servlet.config.annotation.ViewControllerRegistry;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurer;

@Configuration
// SpringBootの場合@EnableWebMvcはつけたら動かなくなる。→ 具体的にはSpringBoot-AutoConfigureの設定が一部無効になるせい。
public class MvcConfig implements WebMvcConfigurer {
	// ViewControllerRegistory はとりあえずHTMLとURLを紐づけするために使用しているだけ。
	// Controllerでの紐づけを追加した場合ここは削除してしまってOK
    public void addViewControllers(ViewControllerRegistry registry) {
        registry.addViewController("/home").setViewName("/balance/show");
        registry.addViewController("/").setViewName("/balance/show");
        registry.addViewController("/hello").setViewName("hello");
    }
    // Filterに関してはここで IntercepterRegistryクラスを使用して設定する。
//    @Override
//    public void addInterceptors(InterceptorRegistry registry) {
//        registry.addInterceptor(new CustomHandlerInterceptor())
//                .addPathPatterns("/**") // 適用対象のパス(パターン)を指定する
//                .excludePathPatterns("/static/**"); // 除外するパス(パターン)を指定する
//    }
}