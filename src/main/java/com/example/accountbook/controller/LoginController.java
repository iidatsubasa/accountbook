package com.example.accountbook.controller;

import javax.servlet.http.HttpSession;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.AuthenticationException;
import org.springframework.security.web.WebAttributes;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.servlet.ModelAndView;

import com.example.accountbook.service.UserService;

@Controller
public class LoginController {



	@Autowired
	HttpSession session;

	@Autowired
	UserService userService;

	// 【ログイン】
	@GetMapping("/login")
	public ModelAndView login() {
		ModelAndView mav = new ModelAndView();
		mav.setViewName("/login");
		return mav;
	}

	// 【ログインエラー】
	@GetMapping("/login-error")
	public ModelAndView loginError() {
		ModelAndView mav = new ModelAndView();
		AuthenticationException ex = (AuthenticationException) session
				.getAttribute(WebAttributes.AUTHENTICATION_EXCEPTION);
		 if (ex != null) {
             mav.addObject("showErrorMsg", true);
             mav.addObject("errorMsg", ex.getMessage());
		 }
		mav.addObject("loginError", true);
		mav.setViewName("/login");
		return mav;
	}
}

