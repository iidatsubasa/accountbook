package com.example.accountbook.controller;


import java.sql.Timestamp;
import java.text.ParseException;
import java.util.List;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.annotation.AuthenticationPrincipal;
import org.springframework.stereotype.Controller;
import org.springframework.util.StringUtils;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.servlet.ModelAndView;

import com.example.accountbook.bean.DateBean;
import com.example.accountbook.bean.Detail;
import com.example.accountbook.entity.Balance;
import com.example.accountbook.entity.ExtendedBalance;
import com.example.accountbook.entity.LoginUser;
import com.example.accountbook.repository.BalanceRepository.BalanceInterface;
import com.example.accountbook.service.BalanceService;
import com.example.accountbook.service.CategoryService;
import com.example.accountbook.service.DateService;
import com.example.accountbook.service.SelectedService;

@Controller
@RequestMapping("/premiere")
public class BalanceController {
	@Autowired
	BalanceService balanceService;

	@Autowired
	CategoryService categoryService;

	@Autowired
	DateService dateService;

	@Autowired
	SelectedService selectedService;

	//明細一覧画面表示
	@GetMapping("/balance")
	public ModelAndView showBalance(@AuthenticationPrincipal LoginUser loginUser) throws ParseException {
		ModelAndView mav = new ModelAndView();
		//先月翌月処理
		DateBean dateBean = new DateBean();
		dateService.CalendarService(dateBean);

		int userId = loginUser.getId();
		//表示セット、リスト取得
		mavAdd(mav, dateBean, userId);
		return mav;
	}

	//明細一覧画面（先月指定）表示
	@GetMapping("/balance/last/{lastYearToMonth}")
	public ModelAndView showBalanceLast(@PathVariable String lastYearToMonth, @AuthenticationPrincipal LoginUser loginUser) throws ParseException {
		ModelAndView mav = new ModelAndView();
		//先月翌月処理
		DateBean dateBean = new DateBean();
		//[yyyyMM]を分割
		dateBean.setYear(lastYearToMonth.substring(0, 4));
		dateBean.setMonth(lastYearToMonth.substring(4, 6));
		dateService.CalendarService(dateBean);
		int userId = loginUser.getId();
		//表示セット、リスト取得
		mavAdd(mav, dateBean, userId);
		return mav;
	}

	//明細一覧画面（翌月指定）表示
	@GetMapping("/balance/next/{nextYearToMonth}")
	public ModelAndView showBalanceNext(@PathVariable String nextYearToMonth, @AuthenticationPrincipal LoginUser loginUser) throws ParseException{
		ModelAndView mav = new ModelAndView();
		DateBean dateBean = new DateBean();
		//[yyyyMM]を分割
		dateBean.setYear(nextYearToMonth.substring(0, 4));
		dateBean.setMonth(nextYearToMonth.substring(4, 6));
		dateService.CalendarService(dateBean);
		int userId = loginUser.getId();
		//表示セット、リスト取得
		mavAdd(mav, dateBean, userId);
		return mav;
	}

	//収支　【登録画面表示】
	@GetMapping("/balance/add")
	public ModelAndView showAddBalance(){
		ModelAndView mav = new ModelAndView();
		Balance balance = new Balance();
		selectedService.select(mav);
		mav.addObject("formModel", balance);
		/*セレクトボックスのフラグ
		0の場合は初期値*/
		mav.addObject("selflag", 0);
		mav.addObject("balanceflag", 1);
		mav.setViewName("balance/add");
		return mav;
	}

	// 収支　【新規登録処理】
	@PostMapping("/balance/add")
	public ModelAndView addBalance(@Valid @ModelAttribute("formModel") Balance balance, BindingResult bindingResult, @AuthenticationPrincipal LoginUser loginUser){
		ModelAndView mav = new ModelAndView();
		if(bindingResult.hasErrors()) {
			selectedService.select(mav);
			mav.addObject("formModel", balance);
			//セレクトボックスのフラグ 0以外は現在の値を保持
			if(balance.getMajorCategoryId() == null || balance.getSubCategoryId() == null) {
				mav.addObject("selflag", 0);
			} else {
				mav.addObject("selflag", 1);
			}
			mav.addObject("balanceflag", balance.getFlag());
			mav.setViewName("balance/add");
			return mav;
		}

		balance.setUserId(loginUser.getId());
		balanceService.saveBalance(balance);
		mav.setViewName("redirect:/premiere/balance");
		return mav;
	}

	@GetMapping("balance/edit")
		public ModelAndView editError(@AuthenticationPrincipal LoginUser loginUser) throws ParseException {
		ModelAndView mav = new ModelAndView();
		//先月翌月処理
		DateBean dateBean = new DateBean();
		dateService.CalendarService(dateBean);
		int userId = loginUser.getId();
		//表示セット、リスト取得
		mavAdd(mav, dateBean, userId);
		mav.addObject("errorMessage", true);
		return mav;
	}


	//明細編集画面表示
	@GetMapping("balance/edit/{id}")
	public ModelAndView showEditBalance(@PathVariable String id, @AuthenticationPrincipal LoginUser loginUser) throws ParseException {
		ModelAndView mav = new ModelAndView();
		DateBean dateBean = new DateBean();
		Balance balance = new Balance();
		dateService.CalendarService(dateBean);
		int userId = loginUser.getId();

		//数値列以外はTOPページへ遷移
		if(StringUtils.hasText(id) && id.matches("^[0-9]*")) {
			Integer intId = Integer.parseInt(id);
			balance = balanceService.findById(intId);
		} else {
			mavAdd(mav, dateBean, userId);
			mav.addObject("errorMessage", true);
			return mav;
		}
		//編集データがない場合TOPページへ遷移
		if(balance == null) {
			mavAdd(mav, dateBean, userId);
			mav.addObject("errorMessage", true);
			return mav;
		}
		selectedService.select(mav);
		mav.addObject("formModel", balance);
		mav.setViewName("balance/edit");
		return mav;
	}

	//明細編集処理
	@PostMapping("balance/edit/{id}")
	public ModelAndView editBalance(@PathVariable Integer id, @ModelAttribute("formModel") Balance balance, @AuthenticationPrincipal LoginUser loginUser) {
		balance.setId(id);
		balance.setUserId(loginUser.getId());
		balance.setUpdatedDate(new Timestamp(System.currentTimeMillis()));
		balanceService.saveBalance(balance);
		return new ModelAndView("redirect:/premiere/balance/");
	}

	//明細削除処理
	@DeleteMapping("/balance/delete/{id}")
	public ModelAndView deleteBalance(@PathVariable Integer id) {
		balanceService.deleteBalance(id);
		return new ModelAndView("redirect:/premiere/balance/");
	}

	//収支一覧
	@GetMapping("/balance/detail")
	public ModelAndView detailBalance(@AuthenticationPrincipal LoginUser loginUser, @ModelAttribute("formModel") Detail detail) throws ParseException {
		ModelAndView mav= new ModelAndView();
		selectedService.selectAll(mav);
		DateBean dateBean = new DateBean();
		int userId = loginUser.getId();
		mavAdd(mav, dateBean, userId);
		List<ExtendedBalance> balances = balanceService.findSerchAll(userId, detail);
		mav.addObject("balances", balances);
		mav.addObject("formModel", detail);
		mav.setViewName("balance/detail");
		return mav;
	}


	//一覧画面リスト取得＆表示処理メソッド
	public ModelAndView mavAdd(ModelAndView mav, DateBean dateBean, int userId) {
		//リスト取得
		List<ExtendedBalance> balances = balanceService.findExtendedAll(userId, dateBean.getYear(), dateBean.getMonth());
		List<BalanceInterface> balanceIncome = balanceService.findByIdBetweenIn(userId, 0, dateBean.getYear(), dateBean.getMonth());
		List<BalanceInterface> balanceExpense = balanceService.findByIdBetweenEx(userId, 1, dateBean.getYear(), dateBean.getMonth());
		/*集計
		nullの場合は0*/
		Integer income = balanceService.findAmount(userId, 0, dateBean.getYear(), dateBean.getMonth());
		if(income == null) {
			income = 0;
		}
		/*集計
		nullの場合は0*/
		Integer expense = balanceService.findAmount(userId, 1, dateBean.getYear(), dateBean.getMonth());
		if(expense == null) {
			expense = 0;
		}
		//合計
		Integer amount = income - expense;
		//表示セット
		mav.addObject("balanceIncome", balanceIncome);
		mav.addObject("balanceExpense", balanceExpense);
		mav.addObject("yearMonth", dateBean.getYearMonth());
		mav.addObject("lastYearToMonth", dateBean.getLastYearToMonth());
		mav.addObject("nextYearToMonth", dateBean.getNextYearToMomth());
		mav.addObject("income", income);
		mav.addObject("expense", expense);
		mav.addObject("amount", amount);
		mav.addObject("balances", balances);
		mav.setViewName("balance/show");
		return mav;
	}
}
