package com.example.accountbook.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.annotation.AuthenticationPrincipal;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.servlet.ModelAndView;

import com.example.accountbook.entity.LoginUser;
import com.example.accountbook.service.BalanceService;
import com.example.accountbook.service.DateService;

@RequestMapping
@Controller
public class MainController {

	@Autowired
	BalanceService balanceService;

	@Autowired
	DateService dateService;

	//ヘッダー表示
	@GetMapping("/header")
	public ModelAndView header(@AuthenticationPrincipal LoginUser loginUser) {
		ModelAndView mav = new ModelAndView();
		mav.addObject("loginUser", loginUser);
		mav.setViewName("header");
		return mav;
	}

}
