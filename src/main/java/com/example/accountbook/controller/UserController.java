package com.example.accountbook.controller;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import javax.servlet.http.HttpSession;
import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.annotation.AuthenticationPrincipal;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.stereotype.Controller;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.servlet.ModelAndView;
import org.thymeleaf.util.StringUtils;

import com.example.accountbook.entity.LoginUser;
import com.example.accountbook.entity.User;
import com.example.accountbook.entity.UserEditForm;
import com.example.accountbook.entity.UserSignupForm;
import com.example.accountbook.service.UserService;

@Controller
public class UserController {

	@Autowired
	HttpSession session;
	@Autowired
	UserService userService;
	BCryptPasswordEncoder encoder = new BCryptPasswordEncoder();

	ModelAndView mav = null;
	List<String> errorMessages = null;

	// ユーザ 【新規登録】
	@GetMapping("/signup")
	public ModelAndView signup() {
		mav = new ModelAndView();
		UserSignupForm user = new UserSignupForm();
		mav.addObject("formModel", user);
		mav.setViewName("signup");
		return mav;
	}

	@PostMapping("/signup")
	public ModelAndView signup(@Valid @ModelAttribute("formModel") UserSignupForm user, BindingResult bindingResult) {
		mav = new ModelAndView();
		errorMessages = new ArrayList<>();

		if(bindingResult.hasErrors()) {
			mav.setViewName("signup");
			return mav;
		}

		// 重複チェック
		User duplicateUser = userService.findByAccount(user.getAccount());
		if(duplicateUser != null) {
			errorMessages.add("アカウント名は既に使用されています。");
			mav.addObject("errorMessages", errorMessages);
			mav.setViewName("signup");
			return mav;
		}
		// 登録処理
		user.setCreatedDate(new Date());
		user.setUpdatedDate(new Date());
		// 暗号化
		user.setPassword(encoder.encode(user.getPassword()));
		userService.saveUser(user);
		mav.setViewName("login");
		return mav;
	}

	// 【ユーザ編集 : GET】
	@GetMapping("/user/edit/{id}")
	public ModelAndView userEdit(@PathVariable String id, @AuthenticationPrincipal LoginUser loginUser) {
		mav = new ModelAndView();
		User user = userService.findById(loginUser.getId());
		user.setPassword(null);
		mav.addObject("formModel", user);
		mav.setViewName("user/edit");
		return mav;
	}

	// 【ユーザ編集 : POST】
	@PostMapping("/user/edit/{id}")
	public ModelAndView userEdit(@PathVariable int id, @Valid @ModelAttribute("formModel") UserEditForm user, BindingResult bindingResult) {
		mav = new ModelAndView();
		errorMessages = new ArrayList<>();
		User loginUser = userService.findById(id);
		User duplicateUser = userService.findByAccount(user.getAccount());
		if(bindingResult.hasErrors()) {
			mav.setViewName("user/edit");
			return mav;
		}
		// 編集前ユーザ情報取得
		User beforeUser = userService.findById(id);
		// 重複チェック
		if(!loginUser.getAccount().equals(user.getAccount())) {
			if(!(duplicateUser == null)) {
				errorMessages.add("アカウント名は既に使用されています。");
				mav.addObject("errorMessages", errorMessages);
				mav.setViewName("user/edit");
				return mav;
			}
		}
		// パスワードサイズ確認
		if(!user.getPassword().isEmpty() && (user.getPassword().length() < 6 || user.getPassword().length() > 20)) {
			errorMessages.add("パスワードは20文字以上6文字以下で入力してください");
			mav.addObject("passwordErrorMessages", errorMessages);
			mav.setViewName("user/edit");
			return mav;
		}

		// 確認用パスワード照合
		if (!(user.getPassword().equals(user.getConfirmPassword()))) {
			errorMessages.add("パスワードと確認用パスワードが一致しません");
			mav.addObject("passwordErrorMessages", errorMessages);
			mav.setViewName("user/edit");
			return mav;
		}
		/* パスワード空欄チェック
		 *  true：現在パスワードをセット
		 *  false：新パスワードを暗号化しセット
		 */
		if (StringUtils.isEmpty(user.getPassword())) {
			user.setPassword(beforeUser.getPassword());
		} else {
			user.setPassword(encoder.encode(user.getPassword()));
		}

		user.setId(id);
		user.setUpdatedDate(new Date());
		userService.editUser(user);
		return new ModelAndView("redirect:/user/edit/" + id);
	}
}
