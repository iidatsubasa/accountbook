package com.example.accountbook.bean;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class DateBean {
	//月
	private String month;
	//年
	private String year;
	//表示用
	private String yearMonth;
	//処理用（yyyyMM）
	private String yearToMonth;
	//処理用（先月を設定yyyyMM）
	private String lastYearToMonth;
	//処理用（翌月を設定yyyyMM）
	private String nextYearToMomth;
}
