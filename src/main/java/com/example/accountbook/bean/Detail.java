package com.example.accountbook.bean;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class Detail {
	private String strDate;
	private String endDate;
	private Integer majorCategoryId;
	private Integer subCategoryId;

}
