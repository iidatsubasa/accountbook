package com.example.accountbook.bean;

import java.util.Date;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class CalendarBean {
	private String year;
	private String month;
	private Date since;
	private Date until;
}
