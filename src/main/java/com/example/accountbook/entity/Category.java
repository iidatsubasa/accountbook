package com.example.accountbook.entity;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

import lombok.Getter;
import lombok.Setter;

@Entity
@Getter
@Setter
@Table(name = "categories")
public class Category {

	@Column(name = "major_category_id")
	private Integer majorCategoryId;

	@Column(name = "major_name")
	private String majorName;

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "sub_category_id")
	private Integer subCategoryId;

	@Column(name = "sub_name")
	private String subName;

	private Integer flag;

}
