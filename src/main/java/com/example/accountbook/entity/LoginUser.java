package com.example.accountbook.entity;

import java.util.Collection;
import java.util.Date;
import java.util.List;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.persistence.Transient;

import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.userdetails.UserDetails;

import lombok.Getter;
import lombok.Setter;

@Entity
@Table(name = "users")
@Getter @Setter
public class LoginUser implements UserDetails {

	@Transient
	private Collection<GrantedAuthority> authorities;

	@Column
	@Id
	private int id;

	@Column(name = "account")
	private String account;

	@Column(name = "name")
	private String userName;

	@Column(name = "password")
	private String password;

	@Column(name = "created_date")
	private Date createdDate;

	@Column(name = "updated_date")
	private Date updatedDate;

	// Constructer
	public LoginUser() {}

	// 以下Override
	@Override
	public Collection<? extends GrantedAuthority> getAuthorities() {
		return authorities;
	}

	@Override
	public String getPassword() {
		return password;
	}

	@Override
	public String getUsername() {
		return userName;
	}

	public LoginUser(String username,
			String password,
			int id, List<GrantedAuthority> authorities) {
		this.userName = username;
        this.password = password;
        this.id = id;
        this.authorities = authorities;
	}

	@Override
	public boolean isAccountNonExpired() {
		return true;
	}

	@Override
	public boolean isAccountNonLocked() {
		return true;
	}

	@Override
	public boolean isCredentialsNonExpired() {
		return true;
	}

	@Override
	public boolean isEnabled() {
		return true;
	}
}
