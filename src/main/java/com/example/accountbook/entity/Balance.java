package com.example.accountbook.entity;
import java.sql.Timestamp;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;

import org.springframework.format.annotation.DateTimeFormat;

import lombok.Getter;
import lombok.Setter;

@Entity
@Table(name = "balances")
@Getter @Setter
public class Balance {
	@Id
	@Column(name = "id")
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private int id;

	@Column(name = "user_id")
	private int userId;

	@Column(name = "amount")
	@NotNull
	private Integer amount;

	@Column(name = "major_category_id")
	@NotNull
	private Integer majorCategoryId;

	@Column(name = "sub_category_id")
	@NotNull
	private Integer subCategoryId;

	@Column(name = "flag")
	private int flag;

	private String memo;

	@Column(name = "accrual_date")
	@DateTimeFormat(pattern = "yyyy-MM-dd")
	@NotNull
	private Date accrualDate;

	@Column(name = "created_date", insertable = false, updatable = false)
	private Timestamp createdDate;

	@Column(name = "updated_date", insertable = false)
	private Timestamp updatedDate;
}
