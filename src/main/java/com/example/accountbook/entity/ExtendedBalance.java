package com.example.accountbook.entity;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Entity
@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
public class ExtendedBalance {
	@Id
	private int id;
	private int userId;
	private Integer amount;
	@Column(name = "major_name")
	private String majorCategoryName;
	@Column(name = "sub_name")
	private String subCategoryName;
	private int flag;
	private String memo;
	private Date accrualDate;
}
