package com.example.accountbook.service;

import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.web.servlet.ModelAndView;

@Service
public class SelectedService {
	@Autowired
	CategoryService categoryService;

	public ModelAndView select(ModelAndView mav){
		List<Integer> majorCategoryId = categoryService.findMajorCategoryId();
		List<String> majorName = categoryService.findMajorName();
		List<Integer> subCategoryId = categoryService.findSubCategoryId();
		List<String> subName = categoryService.findSubName();
		Map<Integer, String> majorIncomeCategories =  new LinkedHashMap<Integer, String>();
		Map<Integer, String> majorExpenseCategories = new LinkedHashMap<Integer, String>();
		Map<Integer, String> subIncomeCategories =  new LinkedHashMap<Integer, String>();
		Map<Integer, String> subExpenseCategories = new LinkedHashMap<Integer, String>();
		for(int i = 0; i < majorCategoryId.size(); i++) {
			if(majorCategoryId.get(i) < 99) {
				majorExpenseCategories.put(majorCategoryId.get(i), majorName.get(i));
			} else {
				majorIncomeCategories.put(majorCategoryId.get(i), majorName.get(i));
			}
		}
		for(int i = 0; i < subCategoryId.size(); i++) {
			if(subCategoryId.get(i) < 9901) {
				subExpenseCategories.put(subCategoryId.get(i), subName.get(i));
			} else {
				subIncomeCategories.put(subCategoryId.get(i), subName.get(i));
			}
		}
		mav.addObject("majorIncomeCategories", majorIncomeCategories);
		mav.addObject("majorExpenseCategories", majorExpenseCategories);
		mav.addObject("subIncomeCategories", subIncomeCategories);
		mav.addObject("subExpenseCategories", subExpenseCategories);
		return mav;
	}

	public ModelAndView selectAll(ModelAndView mav){
		List<Integer> majorCategoryId = categoryService.findMajorCategoryId();
		List<String> majorName = categoryService.findMajorName();
		List<Integer> subCategoryId = categoryService.findSubCategoryId();
		List<String> subName = categoryService.findSubName();
		Map<Integer, String> majorCategories =  new LinkedHashMap<Integer, String>();
		Map<Integer, String> subCategories = new LinkedHashMap<Integer, String>();
		for(int i = 0; i < majorCategoryId.size(); i++) {
			majorCategories.put(majorCategoryId.get(i), majorName.get(i));
		}
		for(int i = 0; i < subCategoryId.size(); i++) {
			subCategories.put(subCategoryId.get(i), subName.get(i));
		}
		mav.addObject("majorCategories", majorCategories);
		mav.addObject("subCategories", subCategories);
		return mav;

	}

}
