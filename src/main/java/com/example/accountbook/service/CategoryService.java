package com.example.accountbook.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.example.accountbook.repository.CategoryRepository;

@Service
@Transactional
public class CategoryService {

	@Autowired
	CategoryRepository categoryRepository;

	public List<Integer> findMajorCategoryId() {
		return categoryRepository.findByMajorCategoryId();
	}

	public List<String> findMajorName() {
		return categoryRepository.findByMajorName();
	}

	public List<Integer> findSubCategoryId() {
		return categoryRepository.findBySubCategoryId();
	}

	public List<String> findSubName() {
		return categoryRepository.findBySubName();
	}
}
