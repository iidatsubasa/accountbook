package com.example.accountbook.service;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;

import com.example.accountbook.entity.LoginUser;
import com.example.accountbook.repository.LoginUserRepository;

@Service
public class UserDetailsServiceImpl implements UserDetailsService {

	@Autowired
	LoginUserRepository loginUserRepository;
	@Autowired
	PasswordEncoder passwordEncoder;
	@Override
	public UserDetails loadUserByUsername(String userName) throws UsernameNotFoundException {

		// user_name から DBのユーザ情報取得
		LoginUser user = loginUserRepository.findByUserName(userName);

		if(user == null) {
			throw new UsernameNotFoundException("User" + userName + "was not found in the database");
		}

		List<GrantedAuthority> grantList = new ArrayList<GrantedAuthority>();

		return new LoginUser(user.getUsername(), user.getPassword(), user.getId(), grantList);
	}
}
