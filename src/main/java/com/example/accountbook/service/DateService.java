package com.example.accountbook.service;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;

import org.springframework.stereotype.Service;

import com.example.accountbook.bean.CalendarBean;
import com.example.accountbook.bean.DateBean;

@Service
public class DateService {
	public DateBean CalendarService(DateBean dateBean) throws ParseException{
		Calendar calendar = Calendar.getInstance();
		//calendarは0~11を返すため、プラス1処理
		//月の取得
		//TOPページのみtrue判定
		if(dateBean.getYear() == null){
			dateBean.setMonth(Integer.toString(calendar.get(Calendar.MONTH) + 1));
		}
		//年の取得
		//TOPページのみtrue判定
		if(dateBean.getYear() == null) {
			dateBean.setYear(Integer.toString(calendar.get(Calendar.YEAR)));
		}
		//表示用処理
		dateBean.setYearMonth("[" + dateBean.getYear() + "年" + dateBean.getMonth() + "月]の収支一覧");
		//フォーマット作成
		SimpleDateFormat sdf = new SimpleDateFormat("yyyyMM");
		dateBean.setYearToMonth(dateBean.getYear() + dateBean.getMonth());
		//Date型へ変換
		Date date = sdf.parse(dateBean.getYearToMonth());
		dateBean.setYearToMonth(new SimpleDateFormat("yyyyMM").format(date));
		//先月処理
		calendar.setTime(date);
		calendar.add(Calendar.MONTH, -1);
		dateBean.setLastYearToMonth(sdf.format(calendar.getTime()));
		//翌月処理
		calendar.setTime(date);
		calendar.add(Calendar.MONTH, +1);
		dateBean.setNextYearToMomth(sdf.format(calendar.getTime()));
		return dateBean;
	}

	public CalendarBean betweenService(CalendarBean calendarBean){
		//フォーマット作成
		SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
		Calendar calendar = Calendar.getInstance();
		//現在の年月を取得
		if(calendarBean.getYear() == null && calendarBean.getMonth() == null) {
			calendarBean.setMonth(Integer.toString(calendar.get(Calendar.MONTH) + 1));
			calendarBean.setYear(Integer.toString(calendar.get(Calendar.YEAR)));
		}
		//月末取得
		calendar.set(Calendar.YEAR, Integer.parseInt(calendarBean.getYear()));
		calendar.set(Calendar.MONTH, Integer.parseInt(calendarBean.getMonth()) - 1);
		String nextMonth = Integer.toString(calendar.getActualMaximum(Calendar.DATE));
		//検索用処理
		try {
			calendarBean.setSince(sdf.parse(calendarBean.getYear() + "-" + calendarBean.getMonth() + "-01"));
			calendarBean.setUntil(sdf.parse(calendarBean.getYear() + "-" + calendarBean.getMonth() + "-" + nextMonth));
		} catch (ParseException e) {
			e.printStackTrace();
		}
		return calendarBean;
	}

}

