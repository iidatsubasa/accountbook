package com.example.accountbook.service;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.example.accountbook.bean.CalendarBean;
import com.example.accountbook.bean.Detail;
import com.example.accountbook.entity.Balance;
import com.example.accountbook.entity.ExtendedBalance;
import com.example.accountbook.repository.BalanceRepository;
import com.example.accountbook.repository.BalanceRepository.BalanceInterface;
import com.example.accountbook.repository.ExtendedBalanceRepository;

@Service
@Transactional
public class BalanceService {
	@Autowired
	BalanceRepository balanceRepository;

	@Autowired
	ExtendedBalanceRepository extendedBalanceRepository;

	@Autowired
	DateService dateService;

	public void saveBalance(Balance balance) {
		balanceRepository.save(balance);
	}

	public List<Balance> findAll(){
		return balanceRepository.findAll();
	}

	public Balance findById(Integer id) {
		return balanceRepository.findById(id).orElse(null);
	}

	public void deleteBalance(Integer id) {
		balanceRepository.deleteById(id);
	}

	//TOP画面グラフ（支出）
	public List<BalanceInterface> findByUserIdGroupByCategoryIdEx(int userId, int flag) {
		CalendarBean calendarBean = new CalendarBean();
		dateService.betweenService(calendarBean);
		return balanceRepository.findByUserIdGroupByCategoryIdEx(userId, flag, calendarBean.getSince(), calendarBean.getUntil());
	}

	//TOP画面グラフ(収入）
	public List<BalanceInterface> findByUserIdGroupByCategoryIdIn(int userId, int flag) {
		CalendarBean calendarBean = new CalendarBean();
		dateService.betweenService(calendarBean);
		return balanceRepository.findByUserIdGroupByCategoryIdIn(userId, flag, calendarBean.getSince(), calendarBean.getUntil());
	}

	//一覧画面リスト（収支）
	public List<ExtendedBalance> findExtendedAll(int uesrId, String year, String month){
		CalendarBean calendarBean = new CalendarBean();
		calendarBean.setYear(year);
		calendarBean.setMonth(month);
		dateService.betweenService(calendarBean);
		return extendedBalanceRepository.findAll(uesrId, calendarBean.getSince(), calendarBean.getUntil());
	}

	//一覧画面グラフ（支出）
	public List<BalanceInterface> findByIdBetweenEx(int userId, int flag, String year, String month) {
		CalendarBean calendarBean = new CalendarBean();
		calendarBean.setYear(year);
		calendarBean.setMonth(month);
		dateService.betweenService(calendarBean);
		return balanceRepository.findByUserIdGroupByCategoryIdEx(userId, flag, calendarBean.getSince(), calendarBean.getUntil());
	}

	//一覧画面グラフ（収入）
	public List<BalanceInterface> findByIdBetweenIn(int userId, int flag, String year, String month) {
		CalendarBean calendarBean = new CalendarBean();
		calendarBean.setYear(year);
		calendarBean.setMonth(month);
		dateService.betweenService(calendarBean);
		return balanceRepository.findByUserIdGroupByCategoryIdIn(userId, flag, calendarBean.getSince(), calendarBean.getUntil());
	}

	public Integer findAmount(int userId, int flag, String year, String month) {
		CalendarBean calendarBean = new CalendarBean();
		calendarBean.setYear(year);
		calendarBean.setMonth(month);
		dateService.betweenService(calendarBean);
		return balanceRepository.findAmount(userId, flag, calendarBean.getSince(), calendarBean.getUntil());

	}

	public List<ExtendedBalance> findSerchAll(int uesrId, Detail detail) throws ParseException {
		Date since = null;
		Date until = null;
		SimpleDateFormat sdf = new SimpleDateFormat( "yyyy-MM-dd" );
		//初期値指定
		//開始日
		if(detail.getStrDate() == null || detail.getStrDate().isEmpty()) {
			String strDate = "2020-01-01";
			since = sdf.parse(strDate);
		} else {
			since = sdf.parse(detail.getStrDate());
		}
		//終了日
		if(detail.getEndDate() == null || detail.getEndDate().isEmpty()) {
			Calendar calendar = Calendar.getInstance();
			String endDate = sdf.format(calendar.getTime());
			until = sdf.parse(endDate);
		} else {
			until = sdf.parse(detail.getEndDate());
		}

		if(detail.getMajorCategoryId() == null) {
			//majorカテゴリ未入力は全件取得
			return extendedBalanceRepository.findAll(uesrId, since, until);
		} else if(detail.getSubCategoryId() == null) {
			return extendedBalanceRepository.findAllWhereMajorCategory(uesrId, detail.getMajorCategoryId(), since, until);
		} else {
			return extendedBalanceRepository.findAllWhereSubCategory(uesrId, detail.getSubCategoryId(), since, until);
		}

	}
}
