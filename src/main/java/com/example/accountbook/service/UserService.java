package com.example.accountbook.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.example.accountbook.entity.LoginUser;
import com.example.accountbook.entity.User;
import com.example.accountbook.entity.UserEditForm;
import com.example.accountbook.entity.UserSignupForm;
import com.example.accountbook.repository.EditUserRepository;
import com.example.accountbook.repository.LoginUserRepository;
import com.example.accountbook.repository.SignupUserRepository;

@Service
public class UserService {

	@Autowired
	SignupUserRepository signupUserRepository;
	@Autowired
	EditUserRepository editUserRepository;
	@Autowired
	LoginUserRepository loginUserRepository;

	// 【ログイン】
	public LoginUser findByAccount(LoginUser user) {
		return loginUserRepository.findByUserName(user.getUsername());
	}
	// 【重複チェック】
	public UserEditForm findByAccount(String account) {
		return editUserRepository.findByAccount(account);
	}
	// 【新規登録】
	public void saveUser(UserSignupForm user) {
		signupUserRepository.save(user);
	}
	// 【ID検索】
	public User findById(int id) {
		return signupUserRepository.findById(id).orElse(null);
	}
	// 【ユーザ編集】
	public void editUser(UserEditForm user) {
		editUserRepository.save(user);
	}
}
